import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';


const routes: Routes = [
  // { path: '', redirectTo: 'login', pathMatch: 'full'},
  // { path: 'login', loadChildren: './pages/login/login.module#LoginPageModule' }

  {
    path: '',
    loadChildren: () =>
      import('./pages/login/login.module').then((m) => m.LoginPageModule),
  },
  {
    path: 'home',
    loadChildren: () =>
      import('./pages/home/home.module').then((m) => m.HomePageModule),
  },
  {
    path: 'establishment-home',
    loadChildren: () => import('./pages/establishment-home/establishment-home.module').then(m => m.EstablishmentHomePageModule)
  },
  {
    path: 'menu-restaurante',
    loadChildren: () => import('./pages/menu-restaurante/menu-restaurante.module').then( m => m.MenuRestaurantePageModule)
  },
  {
    path: 'home',
    children: [
      {
        path: '',
        loadChildren: () => import('./pages/home/home.module').then(m => m.HomePageModule)
      },
      {
        path: ':restauranteId',
        loadChildren: () => import('./pages/home/detalle-rest/detalle-rest.module').then(m => m.DetalleRestPageModule)
      }
    ]
  },
  {
    path: 'edit-user',
    loadChildren: () => import('./pages/edit-user/edit-user.module').then(m => m.EditUserPageModule)
  }

];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules }),
  ],
  exports: [RouterModule],
})
export class AppRoutingModule { }
