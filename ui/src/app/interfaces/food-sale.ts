export interface FoodSale {
  descripcion: string;
  direccion: string;
  imagen: string;
  nombre: string;
  puntuacion: number;
  ubicacion: {
    geogash: string;
    geopoint: { _latitude: number; _longitud: number };
  };
}
