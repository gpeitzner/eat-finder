export interface Platillo {
    Nombre: string;
    Descripcion: string;
    Precio: number;
    id: string;
}
