import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { EstablishmentHomePageRoutingModule } from './establishment-home-routing.module';

import { EstablishmentHomePage } from './establishment-home.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    EstablishmentHomePageRoutingModule
  ],
  declarations: [EstablishmentHomePage]
})
export class EstablishmentHomePageModule {}
