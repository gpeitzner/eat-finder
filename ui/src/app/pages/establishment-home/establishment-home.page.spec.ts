import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';
import { EstablishmentHomePage } from './establishment-home.page';
import { HttpClient, HttpHandler } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { AuthService } from '../../services/auth.service';
import { IonicStorageModule } from '@ionic/storage';
import { SafariViewController } from '@ionic-native/safari-view-controller/ngx';
import { By } from '@angular/platform-browser';
import { EstablishmentService } from '../../services/establishment.service';

describe('EstablishmentHomePage', () => {
  let component: EstablishmentHomePage;
  let fixture: ComponentFixture<EstablishmentHomePage>;
  let service: EstablishmentService;
  const restaurant = {
    nombre: 'test Restaurant',
    descripcion: 'descripcio',
    direccion: 'direccion',
    imagen: 'imagen',
    puntuacion: 5,
    ubicacion: {
      geogash: '54Ds',
      geopoint: {
        _latitude: 1,
        _longitud: 1,
      },
    },
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [EstablishmentHomePage],
      imports: [RouterModule.forRoot([]), IonicStorageModule.forRoot()],
      providers: [HttpClient, HttpHandler, AuthService, SafariViewController],
    }).compileComponents();

    fixture = TestBed.createComponent(EstablishmentHomePage);
    component = fixture.componentInstance;
    component.mainRestaurant = restaurant;
    fixture.detectChanges();
    service = TestBed.inject(EstablishmentService);
  }));

  it('should render restaurant', () => {
    const bannerElement: HTMLElement = fixture.nativeElement;
    const name = bannerElement.querySelector('#restaurant-name');
    const description = bannerElement.querySelector('#restaurant-description');
    const address = bannerElement.querySelector('#restaurant-address');
    const score = bannerElement.querySelector('#restaurant-score');
    const image = bannerElement.querySelector('#restaurant-image');
    expect(name.textContent).toMatch(restaurant.nombre);
    expect(description.textContent).toMatch(restaurant.descripcion);
    expect(address.textContent).toMatch(restaurant.direccion);
    expect(score.textContent).toMatch(restaurant.puntuacion + '');
    expect(image).toBeNull();
  });

  it('should render edit restaurant', () => {
    component.editEnabled = true;
    fixture.detectChanges();

    const name = fixture.debugElement.query(By.css('#restaurant-name-input'));
    const description = fixture.debugElement.query(
      By.css('#restaurant-description-input')
    );
    const address = fixture.debugElement.query(
      By.css('#restaurant-address-input')
    );
    const image = fixture.debugElement.query(By.css('#restaurant-image-input'));
    expect(name.nativeElement.value).toMatch(restaurant.nombre);
    expect(description.nativeElement.value).toMatch(restaurant.descripcion);
    expect(address.nativeElement.value).toMatch(restaurant.direccion);
    expect(image.nativeElement.value).toMatch(restaurant.imagen);
  });
});


