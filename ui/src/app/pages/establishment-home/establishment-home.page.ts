import { Component, OnInit } from '@angular/core';
import { EstablishmentService } from '../../services/establishment.service';
import { FoodSale } from '../../interfaces/food-sale';
import { AuthService } from '../../services/auth.service';
import { DataService } from '../../services/data.service';

export interface Promotion {
  id: string;
  description: string;
}

@Component({
  selector: 'app-establishment-home',
  templateUrl: './establishment-home.page.html',
  styleUrls: ['./establishment-home.page.scss'],
})
export class EstablishmentHomePage implements OnInit {
  constructor(
    private establishmentService: EstablishmentService, // private authService: AuthService
    private dataService: DataService
  ) { }
  mainRestaurant: FoodSale;
  promotions: Promotion[] = [];
  promotionDescription: string;
  promotionId: number = this.promotions.length;
  editEnabled = false;
  public datos: any;
  ngOnInit() {
    this.establishmentService.getFoodSale(1).subscribe(
      (result) => {
        console.log(result);
        this.mainRestaurant = result[0];
      },
      () => { }
    );
    this.getFoodSalePromotions(1);
    this.datos = this.dataService.getComentarios('0');
  }

  editPromotion(id: string, description: string): Promise<any> {
    return new Promise((resolve, reject) => {
      this.establishmentService
        .editFoodSalePromotion(1, id, description)
        .subscribe(
          () => {
            this.getFoodSalePromotions(1);
            resolve();
          },
          () => {
            reject();
          }
        );
    });
  }

  deletePromotion(id: string): Promise<any> {
    return new Promise((resolve, reject) => {
      this.establishmentService.deleteFoodSalePromotion(1, id).subscribe(
        () => {
          this.getFoodSalePromotions(1);
          resolve();
        },
        () => {
          reject();
        }
      );
    });
  }

  addPromotion(description: string): Promise<any> {
    return new Promise((resolve, reject) => {
      this.establishmentService.addFoodSalePromotion(1, description).subscribe(
        () => {
          this.promotionDescription = '';
          this.getFoodSalePromotions(1);
          resolve();
        },
        () => {
          reject();
        }
      );
    });
  }

  getFoodSalePromotions(id: number): Promise<any> {
    return new Promise((resolve, reject) => {
      this.establishmentService.getFoodSalePromotions(id).subscribe(
        (result: Promotion[]) => {
          this.promotions = result;
          resolve();
        },
        () => {
          resolve();
        }
      );
    });
  }

  enableEdit() {
    if (this.editEnabled) {
      const { descripcion, direccion, imagen, nombre } = this.mainRestaurant;
      console.log(this.mainRestaurant);
      this.establishmentService
        .updateRestaurant(
          1,
          /*this.authService.user.app_metadata.restaurantId*/
          descripcion,
          direccion,
          imagen,
          nombre
        )
        .subscribe(
          (result) => {
            console.log(result);
            console.log('DSDSD');
          },
          () => {
            console.log('Loaded');
          }
        );
    }
    this.editEnabled = !this.editEnabled;

    /*
    this.dataService.getComentarios("0").subscribe(
      (data) => {
        console.log("DATOS UN RESTAURANTE: ");
        console.log(data);
        this.datos = data;
      },
      () => {}
    );*/
  }
}
