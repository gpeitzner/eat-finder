import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { ModifyuserService } from '../../services/modifyuser.service';
import { AuthService } from '../../services/auth.service';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-edit-user',
  templateUrl: './edit-user.page.html',
  styleUrls: ['./edit-user.page.scss'],
})
export class EditUserPage implements OnInit {

  constructor(
    public alertController: AlertController,
    private modifyService: ModifyuserService, // private authService: AuthService
    private authService: AuthService
  ) { }
  @ViewChild('passwordEyeRegister', { read: ElementRef }) passwordEye: ElementRef;
  passwordTypeInput = 'password';
  // Variable para cambiar dinamicamente el tipo de Input que por defecto sera 'password'
  iconpassword = 'eye-off';
  email: string;
  nombre: string;
  apellido: string;
  password: string;

  nuevoNombre: string;
  nuevoApellido: string;
  nuevaPassword: string;


  ngOnInit() {
    this.modifyService.getUser(this.authService.user.email).subscribe(
      (result) => {
        console.log(result[0]);
        this.email = result[0].id;
        this.nombre = result[0].nombre;
        this.apellido = result[0].apellido;
        this.password = result[0].password;
        console.log(this.email, this.nombre, this.apellido, this.password);
      },
      () => { }
    );

  }

  async presentAlert() {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Editar Datos',
      subHeader: '',
      message: 'Se actualizaron sus datos exitosamente',
      buttons: ['OK']
    });

    await alert.present();
  }


  togglePasswordMode() {
    this.passwordTypeInput = this.passwordTypeInput === 'text' ? 'password' : 'text';
    this.iconpassword = this.iconpassword === 'eye-off' ? 'eye' : 'eye-off';
  }

  editUser() {

    this.modifyService.editUser(this.authService.user.email, this.nuevoNombre, this.nuevoApellido, this.nuevaPassword).subscribe(
      (result) => {
        console.log(result);
        this.presentAlert();
      },
      (error) => {
        console.error(error);

      }
    );
  }

}
