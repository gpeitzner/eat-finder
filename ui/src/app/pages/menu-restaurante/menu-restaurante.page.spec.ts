import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { MenuRestaurantePage } from './menu-restaurante.page';
import { HttpClient, HttpHandler } from '@angular/common/http';

import { DataService } from '../../services/data.service';
import { Observable, of } from 'rxjs';
import { Platillo } from '../../interfaces/platillo';
import { By } from '@angular/platform-browser';

describe('MenuRestaurantePage', () => {
  let component: MenuRestaurantePage;
  let fixture: ComponentFixture<MenuRestaurantePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MenuRestaurantePage ],
      imports: [IonicModule.forRoot()],
      providers: [HttpClient, HttpHandler, {provide: DataService, useClass: DataServiceMock}],
    }).compileComponents();

    fixture = TestBed.createComponent(MenuRestaurantePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should defined', () => {
    expect(component).toBeDefined();
  });

  it('should exist menu', () => {
    expect(component.comidas).not.toBeUndefined();
  });

  it('should have a title', () => {
    const title = fixture.debugElement.query(By.css('ion-title')).nativeElement;
    console.log(title.innerHTML);
    expect(title.innerHTML).toBe('Menu Restaurante');
  });

  it('should have a header', () => {
    fixture.detectChanges();
    const navbar = fixture.debugElement.query(By.css('ion-header')).nativeElement;

    expect(navbar.innerHTML).not.toBeNull();
  });

  it('should exist data', () => {
    component.comidas = mockMenuRestaurante;
    expect(component.comidas.length).toBeGreaterThan(0);
  });

  it('should have a meals', () => {
    fixture.detectChanges();
    expect(component.comidas.length).toBeGreaterThan(0);
  });

  it('should have a meals container', () => {
    fixture.detectChanges();
    const mealsContainer = fixture.debugElement.nativeElement.querySelector('#mealsContainer');
    expect(mealsContainer.innerHTML).not.toBeNull();
  });

  it('should have the same number of meals list in meals containter', () => {
    fixture.detectChanges();
    const listOfMealsInContainer = fixture.debugElement.queryAll(By.css('ion-content ion-row ion-col ion-card'));
    expect(listOfMealsInContainer.length).toEqual(component.comidas.length);
  });

});

// DummyServiceMock
class DataServiceMock {
  public getComidas(): Observable<Platillo[]> {
    return of(mockMenuRestaurante);
  }
}

// Mock Meals
const mockMenuRestaurante = [
  {
      Descripcion: 'La hamburguesa coronada con champiñones, cebollas salteadas, queso Suizo y mayonesa.',
      Nombre: 'MUSHROOM ONION SWISS BURGER',
      Precio: 81,
      id: '1'
  },

  {
  Descripcion: 'Hamburguesa con extra queso y salsa picante',
      Nombre: 'Hamburgesa Extra',
      Precio: 34,
      id: '1_Hamburgesa Extra'
  },
  {
      Descripcion: 'Hamburgesa simple',
      Nombre: 'Hamburgesa',
      Precio: 47,
      id: '1_RIBS & WINGS BOX'
  },
  {
      Descripcion: 'La hamburguesa con champiñones, cebollas salteadas, queso Suizo y mayonesa.',
      Nombre: 'MUSHROOM ONION SWISS BURGER 2',
      Precio: 82,
      id: '2'
  }
];
