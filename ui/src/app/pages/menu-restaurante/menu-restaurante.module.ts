import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MenuRestaurantePageRoutingModule } from './menu-restaurante-routing.module';

import { MenuRestaurantePage } from './menu-restaurante.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MenuRestaurantePageRoutingModule
  ],
  declarations: [MenuRestaurantePage]
})
export class MenuRestaurantePageModule {}
