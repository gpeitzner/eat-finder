import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MenuRestaurantePage } from './menu-restaurante.page';

const routes: Routes = [
  {
    path: '',
    component: MenuRestaurantePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MenuRestaurantePageRoutingModule {}
