import { Component, OnInit } from '@angular/core';
import { DataService } from '../../services/data.service';
import { Platillo } from '../../interfaces/platillo';
@Component({
  selector: 'app-menu-restaurante',
  templateUrl: './menu-restaurante.page.html',
  styleUrls: ['./menu-restaurante.page.scss'],
})
export class MenuRestaurantePage implements OnInit {

  comidas: Platillo[] = [];
  nombreNuevo: string;
  precioNuevo: string;
  descripcionNueva: string;
  idRestaurante: string;

  constructor( private dataService: DataService ) {
    this.idRestaurante = '1';
  }

  ngOnInit() {
    this.cargarMenu(this.idRestaurante);
  }

  cargarMenu(idRestaurante: string) {
    this.dataService
      .getComidas(idRestaurante)
      .subscribe(
        (result) => {
          this.comidas = result;
        },
        () => { }
    );
  }

  editar(comida: Platillo) {
    const cn: Platillo = {
      Nombre: this.nombreNuevo,
      Descripcion: this.descripcionNueva,
      // tslint:disable-next-line: radix
      Precio: parseInt(this.precioNuevo),
      id: comida.id
    };
    this.dataService.updateComidaRestaurante(this.idRestaurante, cn);
  }

  agregar(){
    const cn = {
      Nombre: this.nombreNuevo,
      Descripcion: this.descripcionNueva,
      // tslint:disable-next-line: radix
      Precio: parseInt(this.precioNuevo)
    };
    this.dataService.addComidaRestaurante(this.idRestaurante, cn);
  }
}
