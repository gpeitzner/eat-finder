import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DetalleRestPageRoutingModule } from './detalle-rest-routing.module';

import { DetalleRestPage } from './detalle-rest.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DetalleRestPageRoutingModule
  ],
  declarations: [DetalleRestPage]
})
export class DetalleRestPageModule {}
