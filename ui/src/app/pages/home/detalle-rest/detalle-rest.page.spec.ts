import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { DetalleRestPage } from './detalle-rest.page';
import { AuthService } from '../../../services/auth.service';
import { IonicStorageModule } from '@ionic/storage';
import { SafariViewController } from '@ionic-native/safari-view-controller/ngx';

import { RouterModule } from '@angular/router';
import { HttpClient, HttpHandler } from '@angular/common/http';

describe('DetalleRestPage', () => {
  let component: DetalleRestPage;
  let fixture: ComponentFixture<DetalleRestPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [DetalleRestPage],
      imports: [RouterModule.forRoot([]),
      IonicStorageModule.forRoot()],
      providers: [HttpClient, HttpHandler, AuthService, SafariViewController],
    }).compileComponents();

    fixture = TestBed.createComponent(DetalleRestPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));
});
