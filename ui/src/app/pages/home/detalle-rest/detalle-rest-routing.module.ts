import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DetalleRestPage } from './detalle-rest.page';

const routes: Routes = [
  {
    path: '',
    component: DetalleRestPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DetalleRestPageRoutingModule {}
