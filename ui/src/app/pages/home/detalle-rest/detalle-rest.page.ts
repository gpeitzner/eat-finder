import { Component, OnInit } from '@angular/core';
import { DataService } from '../../../services/data.service';
import { ActivatedRoute } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-detalle-rest',
  templateUrl: './detalle-rest.page.html',
  styleUrls: ['./detalle-rest.page.scss'],
})
export class DetalleRestPage implements OnInit {
  myRate = 1;
  apiServer =
    'https://us-central1-eatfinder.cloudfunctions.net/restaurants';
  foodSaleId: number;

  constructor(
    private activatedrouter: ActivatedRoute,
    private dataService: DataService,
    private httpClient: HttpClient,
    private activatedRoute: ActivatedRoute,
    private authService: AuthService
  ) { }
  public datos: any = [
    {
      id: '0',
      nombre: 'RESTAURANTE',
      imagen:
        'https://cnnespanol.cnn.com/wp-content/uploads/2019/01/Galerias-nuevos-restaurantes-2019-CNN-1.jpg?quality=100&strip=info&w=320&h=240&crop=1',
      descripcion:
        'Cadena de pizzerías de ambiente familiar conocida por su variedad de pizzas al gusto del consumidor',
      puntuacion: 4.5,
    },
  ];
  public comentarios: any;

  ngOnInit() {
    this.activatedrouter.paramMap.subscribe(
      (paramMap) => {
        const recipeId = paramMap.get('restauranteId');
        this.foodSaleId = parseInt(recipeId, 10);
        this.dataService.getRestaurante(recipeId).subscribe(
          (data) => {
            this.datos = data;
            this.httpClient
              .get(
                this.apiServer +
                '/' +
                this.authService.user.email +
                '/rate/' +
                recipeId
              )
              .subscribe(
                (sData: { Valor: string }) => {
                  this.myRate = parseInt(sData.Valor, 10);
                },
                () => { }
              );
          },
          () => { }
        );
      },
      () => { },
      () => { }
    );
    this.comentarios = this.dataService.getComentarios('0');
  }

  updateRate(): void {
    this.httpClient
      .post(
        this.apiServer +
        '/' +
        this.authService.user.email +
        '/rate/' +
        this.foodSaleId,
        { value: this.myRate }
      )
      .subscribe(
        () => { },
        () => { }
      );
  }
}
