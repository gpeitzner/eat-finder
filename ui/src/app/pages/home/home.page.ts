import { Component, OnInit } from '@angular/core';
import { MenuController } from '@ionic/angular';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { LocationService } from 'src/app/services/location.service';
import { DataService } from '../../services/data.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {
  public rest: any = [];
  constructor(
    private menu: MenuController,
    private geolocation: Geolocation,
    private locationService: LocationService,
    private dataService: DataService
  ) {}

  distance = 20;
  search = '';

  ngOnInit() {
    // this.updateUserLocation();
  }

  handleSearch(): void {
    this.dataService.getByName(this.search).subscribe((data) => {
      this.rest = data;
    });
  }

  updateUserLocation() {
    this.geolocation
      .getCurrentPosition()
      .then((resp) => {
        this.locationService.setLocation(
          resp.coords.latitude,
          resp.coords.longitude
        );
      })
      .catch((error) => {
        console.log('Error getting location', error);
      });
    console.log('Datos de la ubicación: ');
    const posiciones: any = this.locationService.getLocation();
    if (
      posiciones.latitude !== undefined &&
      posiciones.longitude !== undefined
    ) {
      console.log('Longitud:');
      console.log(posiciones.longitude);
      console.log('Latitude:');
      console.log(posiciones.latitude);
      this.dataService
        .getData(posiciones.latitude, posiciones.longitude, this.distance)
        .subscribe((data) => {
          console.log('DATOS');
          console.log(data);
          this.rest = data;
        });
    }

    console.log(this.locationService.getLocation());
  }
}
