
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Platillo } from '../interfaces/platillo';

@Injectable({
  providedIn: 'root',
})
export class DataService {

  constructor(private http: HttpClient) {}

  getData(latitud: string, longitud: string, distancia: number) {
    console.log('Haciendo get a la ruta: ');
    console.log(
      'https://us-central1-eatfinder.cloudfunctions.net/restaurants?lat=' +
        latitud +
        '&lng=' +
        '&range=' +
        distancia
    );
    return this.http.get(
      'https://us-central1-eatfinder.cloudfunctions.net/restaurants?lat=' +
        latitud +
        '&lng=' +
        longitud +
        '&range=' +
        distancia
    );
    // return this.http.get("/api/");
    // return this.http.get("https://www.reddit.com/r/gifs/top/.json?limit=10&sort=hot");
  }

  getByName(name: string) {
    return this.http.post(
      'https://us-central1-eatfinder.cloudfunctions.net/restaurants/filtroNombre',
      { Nombre: name }
    );
  }

  getRestaurante(id: string){
    return this.http.get(
      'https://us-central1-eatfinder.cloudfunctions.net/restaurants/' + id
    );
  }

  getComidas(id: string): Observable<Platillo[]>{
    return this.http.get<Platillo[]>('https://us-central1-eatfinder.cloudfunctions.net/restaurants/' + id + '/menu');
  }

  updateComidaRestaurante(idRestaurante: string, comida: Platillo){
    // console.log(`se actualizara: ${comida}`);
    this.http.put(
        'https://us-central1-eatfinder.cloudfunctions.net/restaurants/' + idRestaurante + '/menu', comida)
      .subscribe(
        () => {},
        () => {}
      );

  }

  addComidaRestaurante(idRestaurante: string, comida: any){
    this.http.post(
      'https://us-central1-eatfinder.cloudfunctions.net/restaurants/' + idRestaurante + '/menu', comida)
    .subscribe(
      () => {},
      () => {}
    );
  }

  getComentarios(id: string) {
    const dataprueba = [
      {
        correo: 'prueba1@gmail.com',
        valor: 5,
        comentario: 'Muy rica la comida!',
      },
      {
        correo: 'prueba2@gmail.com',
        valor: 2,
        comentario: 'Estaba salada la comida',
      },
      {
        correo: 'prueba3@gmail.com',
        valor: 4,
        comentario: 'Me gustó el lugar',
      },
    ];
    return dataprueba;
  }
}
