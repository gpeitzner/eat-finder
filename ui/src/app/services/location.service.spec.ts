import { TestBed } from '@angular/core/testing';

import { LocationService } from './location.service';

describe('LocationService', () => {
  let service: LocationService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(LocationService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('#setLocation should set a location', () => {
    service.setLocation(14.623263, -90.553874);
    expect(service.getLocation()).toEqual({
      latitude: 14.623263,
      longitude: -90.553874,
    });
  });

  it('#getLocation should return a location', () => {
    service.setLocation(14.623263, -90.553874);
    expect(service.getLocation()).toEqual({
      latitude: 14.623263,
      longitude: -90.553874,
    });
  });
});

describe('Given a location with the next values: latitude 14 and longitud 90', () => {
  let service: LocationService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(LocationService);
  });

  it('When I set the location (latitude and longitude)', () => {
    service.setLocation(14, 90);
  });

  it('Then I get a location with values equals to 14 (latitude) and 90 (longitude)', () => {
    service.setLocation(14, 90);
    expect(service.getLocation()).toEqual({
      latitude: 14,
      longitude: 90,
    });
  });
});
