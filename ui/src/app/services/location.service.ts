import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class LocationService {
  latitude: number;
  longitude: number;

  constructor() { }

  /**
   * Set users latitude and longitude
   * @param latitude user current latitude
   * @param longitude user current longitude
   */
  setLocation(latitude: number, longitude: number): void {
    this.latitude = latitude;
    this.longitude = longitude;
  }

  /**
   * Returns users latitude and longitude
   */
  getLocation(): any {
    return { longitude: this.longitude, latitude: this.latitude };
  }
}
