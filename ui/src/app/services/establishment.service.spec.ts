import { TestBed } from '@angular/core/testing';

import { EstablishmentService } from './establishment.service';
import {
  HttpClient,
  HttpHandler,
  HttpClientModule,
} from '@angular/common/http';

describe('EstablishmentService', () => {
  let service: EstablishmentService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientModule],
      providers: [HttpClient, HttpHandler],
    }).compileComponents();
    service = TestBed.inject(EstablishmentService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('#getFoodSale should return a food sale information', () => {
    expect(service.getFoodSale(2)).toBeDefined();
  });
});
