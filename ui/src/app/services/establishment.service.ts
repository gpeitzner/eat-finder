import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { FoodSale } from '../interfaces/food-sale';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class EstablishmentService {
  apiServer = 'https://us-central1-eatfinder.cloudfunctions.net/';

  constructor(private httpClient: HttpClient) {}

  /**
   * Return food sale information by an observable
   * @param id Food sale id
   */
  getFoodSale(id: number): Observable<FoodSale[]> {
    return this.httpClient.get<FoodSale[]>(
      this.apiServer + 'restaurants/' + id
    );
  }

  /**
   * Return food sales promotions by an observable
   * @param id Food sale id
   */
  getFoodSalePromotions(id: number): Observable<any[]> {
    return this.httpClient.get<any[]>(
      this.apiServer + 'restaurants/promotions/' + id
    );
  }

  /**
   * Add food sales promotion
   * @param id Food sale id
   * @param description Promotion description
   */
  addFoodSalePromotion(id: number, description: string): Observable<any> {
    return this.httpClient.post(
      this.apiServer + 'restaurants/createprom/' + id,
      { descripcion: description }
    );
  }

  /**
   * Edit food sales promotion
   * @param foodSaleId Food sale id
   * @param id Promotion id
   * @param description Promotions description
   */
  editFoodSalePromotion(
    foodSaleId: number,
    id: string,
    description: string
  ): Observable<any> {
    return this.httpClient.put(
      this.apiServer + 'restaurants/modifyprom/' + foodSaleId,
      { promocion: id, descripcion: description }
    );
  }

  /**
   * Dlete food sales promotion
   * @param foodSaleId Food sale id
   * @param id Promotion id
   */
  deleteFoodSalePromotion(foodSaleId: number, id: string): Observable<any> {
    return this.httpClient.post(
      this.apiServer + 'restaurants/remprom/' + foodSaleId,
      {
        promocion: id,
      }
    );
  }

  updateRestaurant(
    id: number,
    descripcion: string,
    direccion: string,
    imagen: string,
    nombre: string
  ): Observable<FoodSale[]> {
    return this.httpClient.put<FoodSale[]>(
      `${this.apiServer}restaurants/modify/${id}`,
      { descripcion, direccion, imagen, nombre }
    );
  }
}
