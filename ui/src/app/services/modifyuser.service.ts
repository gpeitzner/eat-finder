import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ModifyuserService {
  apiServer = 'https://us-central1-eatfinder.cloudfunctions.net/restaurants/User/';
  constructor(private http: HttpClient) {}

  getUser(id: string) {
    return this.http.get(
      this.apiServer + id
    );
  }

  editUser(
    id: string,
    nombre: string,
    apellido: string,
    password: string
  ): Observable<any> {
    return this.http.put(
      this.apiServer + id,
      { Nombre: nombre, Apellido: apellido, Password : password }
    );
  }


}
