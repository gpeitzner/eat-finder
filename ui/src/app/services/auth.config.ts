export const AUTH_CONFIG = {
  // Needed for Auth0 (capitalization: ID):
  clientID: '1sGObGYKask2YRTS0HY9vHmFPuz7z675',
  // Needed for Auth0Cordova (capitalization: Id):
  clientId: '1sGObGYKask2YRTS0HY9vHmFPuz7z675',
  domain: 'eatfinder.us.auth0.com', // e.g., you.auth0.com
  packageIdentifier: 'io.ionic.starter', // config.xml widget ID
};
