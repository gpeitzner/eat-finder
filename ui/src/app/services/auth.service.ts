import { Injectable, NgZone } from '@angular/core';
import { Storage } from '@ionic/storage';
import { SafariViewController } from '@ionic-native/safari-view-controller/ngx';

// Import AUTH_CONFIG, Auth0Cordova, and auth0.js
import { AUTH_CONFIG } from './auth.config';
import Auth0Cordova from '@auth0/cordova';
import * as auth0 from 'auth0-js';
import { Router } from '@angular/router';
import { Platform } from '@ionic/angular';

declare let cordova: any;

@Injectable()
export class AuthService {
  Auth0 = new auth0.WebAuth(AUTH_CONFIG);
  Client = new Auth0Cordova(AUTH_CONFIG);
  accessToken: string;
  user: any;
  loggedIn: boolean;
  loading = true;
  // private router: Router;

  constructor(
    public zone: NgZone,
    private storage: Storage,
    private safariViewController: SafariViewController,
    private router: Router,
    private platform: Platform
  ) {
    this.storage.get('profile').then((user) => (this.user = user));
    this.storage
      .get('access_token')
      .then((token) => (this.accessToken = token));
    this.storage.get('expires_at').then((exp) => {
      if (exp) {
        this.loggedIn = Date.now() < JSON.parse(exp);
        this.loading = false;
      } else {
        this.loggedIn = false;
        this.loading = false;
      }
    });
    // this.router = router;
  }

  login() {
    this.loading = true;
    const options = {
      scope: 'openid profile offline_access',
    };
    if (this.platform.is('desktop') || this.platform.is('mobileweb')) {
      const profile = {
        name: 'user name',
        email: 'mindi.ajpop@gmail.com',
        picture: '',
        app_metadata: {
          restaurantId: '1', // remove to be a normal user
        },
      };
      this.user = profile;

      this.redirectUser();
    } else {
      // Authorize login request with Auth0: open login page and get auth results
      this.Client.authorize(options, (err, authResult) => {
        if (err) {
          this.zone.run(() => (this.loading = false));
          throw err;
        }
        // Set access token
        this.storage.set('access_token', authResult.accessToken);
        this.accessToken = authResult.accessToken;
        // Set access token expiration
        const expiresAt = JSON.stringify(
          authResult.expiresIn * 1000 + new Date().getTime()
        );
        this.storage.set('expires_at', expiresAt);
        // Set logged in
        this.loading = false;
        this.loggedIn = true;
        // Fetch user's profile info
        this.Auth0.client.userInfo(this.accessToken, (error, profile) => {
          if (error) {
            throw error;
          }
          const namespace = 'https://eatfinder.com/metadata';
          profile.app_metadata = profile[namespace];
          delete profile[namespace];
          this.storage
            .set('profile', profile)
            .then((val) => this.zone.run(() => (this.user = profile)));

          this.redirectUser();
        });
      });
    }
  }

  redirectUser() {
    console.log(this.user, 'USER');
    if (
      this.user &&
      this.user.app_metadata &&
      this.user.app_metadata.restaurantId
    ) {
      this.router.navigate(['establishment-home']);
    }
    // redirect to resturant view
    else { this.router.navigate(['home']); }
  }

  logout() {
    this.accessToken = null;
    this.user = null;
    this.loggedIn = false;
    this.safariViewController.isAvailable().then((available: boolean) => {
      const auth0Domain = AUTH_CONFIG.domain;
      const clientId = AUTH_CONFIG.clientId;
      const pkgId = AUTH_CONFIG.packageIdentifier;
      const url = `https://${auth0Domain}/v2/logout?client_id=${clientId}&returnTo=${pkgId}://${auth0Domain}/cordova/${pkgId}/callback`;
      if (available) {
        this.safariViewController
          .show({
            url,
          })
          .subscribe(
            (result: any) => {
              if (result.event === 'opened') { console.log('Opened'); }
              else if (result.event === 'closed') { console.log('Closed'); }

              if (result.event === 'loaded') {
                console.log('Loaded');
                this.storage.remove('profile');
                this.storage.remove('access_token');
                this.storage.remove('expires_at');
                this.safariViewController.hide();
              }
            },
            (error: any) => console.error(error)
          );
      } else {
        // use fallback browser
        cordova.InAppBrowser.open(url, '_system');
      }
    });
  }
}
