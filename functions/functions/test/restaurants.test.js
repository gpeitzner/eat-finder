require("firebase-functions-test")();
const FirestoreMock = require("./firestore.mock");
const GeoFirestoreMock = require("./geofirestore.mock");
const request = require("supertest");
const geofirestore = require("geofirestore");

describe("Restaurants", () => {
  let app;
  const firestoreMock = new FirestoreMock();
  const geoFirestoreMock = new GeoFirestoreMock();

  beforeEach(() => {
    app = require("../restaurants");
    jest.clearAllMocks();
    firestoreMock.reset();
    geoFirestoreMock.reset();

    geofirestore.initializeApp = () => geoFirestoreMock;

    return jest.mock("firebase-admin", () => ({
      initializeApp: jest.fn(),
      firestore: firestoreMock,
    }));
  });

  it("load function", (done) => {
    const query = { lat: 10, lng: 15, range: 10 };

    const expectedResponse = [
      {
        nombre: "McDonald's",
        descripcion:
          "Veterana cadena de comida rápida famosa por sus hamburguesas, patatas fritas y bebidas",
        direccion:
          "51 calle y Avenida Petapa 20-11, zona 12, Ciudad Real, Cdad. de Guatemala",
        puntuacion: "3",
        ubicacion: {
          geohash: "9fxdg6xw3",
        },
        imagen: "",
      },
    ];

    geoFirestoreMock.mockGetReturn = {
      docs: [
        {
          data: () => ({
            Nombre: "McDonald's",
            Descripcion:
              "Veterana cadena de comida rápida famosa por sus hamburguesas, patatas fritas y bebidas",
            Direccion:
              "51 calle y Avenida Petapa 20-11, zona 12, Ciudad Real, Cdad. de Guatemala",
            Puntuacion: "3",
            g: {
              geohash: "9fxdg6xw3",
            },
            Imagen: "",
          }),
        },
      ],
    };

    request(app)
      .get("/")
      .query(query)
      .expect(200, (err, res) => {
        if (!err) {
          expect(res.body).toEqual(expectedResponse);
        }
        done();
      });
  });

  it("load functiond", (done) => {
    const query = { lat: 10, lng: 15, range: 10 };

    const expectedResponse = [
      {
        nombre: "McDonald's",
        descripcion:
          "Veterana cadena de comida rápida famosa por sus hamburguesas, patatas fritas y bebidas",
        direccion:
          "51 calle y Avenida Petapa 20-11, zona 12, Ciudad Real, Cdad. de Guatemala",
        puntuacion: "3",
        ubicacion: {
          geohash: "9fxdg6xw3",
        },
        imagen: "",
      },
    ];

    geoFirestoreMock.mockGetReturn = {
      docs: [
        {
          data: () => ({
            Nombre: "McDonald's",
            Descripcion:
              "Veterana cadena de comida rápida famosa por sus hamburguesas, patatas fritas y bebidas",
            Direccion:
              "51 calle y Avenida Petapa 20-11, zona 12, Ciudad Real, Cdad. de Guatemala",
            Puntuacion: "3",
            g: {
              geohash: "9fxdg6xw3",
            },
            Imagen: "",
          }),
        },
      ],
    };
    request(app)
      .get("/")
      .query(query)
      .expect(200, (err, res) => {
        if (!err) {
          expect(res.body).toEqual(expectedResponse);
        }
        done();
      });
  });


  it("Add restaurant's rate", (done) => {
    const query = { value: 5 };
    const expectedResponse = [
      {
        Correo: "test@gmail.com",
        Restaurante: 1,
        Valor: 5,
      },
    ];

    firestoreMock.mockGetReturn = {
      docs: [
        {
          data: () => ({
            Correo: "test@gmail.com",
            Restaurante: 1,
            Valor: 5,
          }),
        },
      ],
    };

    request(app)
      .post("/test@gmail.com/rate/1")
      .query(query)
      .expect(200, (err, res) => {
        if (!err) {
          expect(res.body).toEqual(expectedResponse);
        }
        done();
      });
  });

  it("hacer put a modificar promocion", (done) => {
    const body = {
      promocion: "UxN66CBlOWW5tamTbPoE",
      descripcion: "Esta es una promocion de prueba modificada",
    };
    request(app)
      .put("/modifyprom/1")
      .set("Content-Type", "application/json")
      .send(body)
      .expect(500)
      .end((err, res) => {
        expect(res.status).toBe(500);
        done();
      });
  });

  it("hacer delete a una promocion", (done) => {
    const body = {
      promocion: "UxN66CBlOWW5tamTbPoE",
    };
    request(app)
      .post("/remprom/1")
      .set("Content-Type", "application/json")
      .send(body)
      .expect(500)
      .end((err, res) => {
        expect(res.status).toBe(500);
        done();
      });
  });

  // TDD de obtener menu en restaurante
  it("obtener el menu de un restaurante", function (done) {
    request("https://us-central1-eatfinder.cloudfunctions.net/restaurants")
      .get("/1/menu")
      .set("Content-Type", "application/json")
      .query()
      .expect(200, done);
  });

  it("Obtener adecuadamente el menu de un restaurante", (done) => {
    const ident = { id: 1 };
    const expectedResponse = [
      {
        Nombre: "Producto 1",
        Descripcion: "platillo X con ingredientes X",
        Precio: 150.5,
        id: "1",
      },
      {
        Nombre: "Platillo 2",
        Descripcion: "Descripcion 2 producto 2",
        Precio: "150.5",
        id: "2",
      },
    ];
    firestoreMock.mockGetReturn = {
      docs: [
        {
          data: () => (
            {
              Nombre: "Producto 1",
              Descripcion: "platillo X con ingredientes X",
              Precio: 150.5,
              id: "1",
            },
            {
              Nombre: "Platillo 2",
              Descripcion: "Descripcion 2 producto 2",
              Precio: "150.5",
              id: "2",
            }
          ),
        },
      ],
    };
    request("https://us-central1-eatfinder.cloudfunctions.net/restaurants")
      .get("/1/menu")
      .set("Content-Type", "application/json")
      .query()
      .expect(200, (err, res) => {
        const { body } = res;
        expect(body); // .toMatchObject(expectedResponse)
        done();
      });
  });

  // TDD de modificar producto en menu de restaurante
  it("Verificar si existe endpoint para modificar", function (done) {
    const producto = {
      Nombre: "MUSHROOM ONION SWISS BURGER",
      Descripcion:
        "La hamburguesa coronada con champiñones, cebollas salteadas, queso Suizo y mayonesa.",
      Precio: 81.0,
      id: "1",
    };

    const expectedResponse = {
      Nombre: "MUSHROOM ONION SWISS BURGER",
      Descripcion:
        "La hamburguesa coronada con champiñones, cebollas salteadas, queso Suizo y mayonesa.",
      Precio: 81.0,
    };

    firestoreMock.mockAddReturn = {
      docs: [
        {
          data: () => ({
            Nombre: "MUSHROOM ONION SWISS BURGER",
            Descripcion:
              "La hamburguesa coronada con champiñones, cebollas salteadas, queso Suizo y mayonesa.",
            Precio: 81.0,
          }),
        },
      ],
    };
    request("https://us-central1-eatfinder.cloudfunctions.net/restaurants")
      .put("/1/menu")
      .set("Content-Type", "application/json")
      .send(producto)
      .expect(201, (err, res) => {
        const { body } = res;
        expect(res.body).toMatchObject(expectedResponse);
        done();
      });
  });
  // TDD de agregar productos al menú de un restaurante.
  it("Verificar si existe endpoint para agregar producto", function (done) {
    const producto = {
      Nombre: "RIBS & WINGS BOX",
      Descripcion:
        "Baby Back Ribs BBQ o Whiskey Glaze, Buffalo Wings, Friday’s fries, acompañadas con coleslaw y nuestros aderezos Blue Cheese y Buffalo.",
      Precio: 400.0,
    };

    const expectedResponse = {
      Nombre: "RIBS & WINGS BOX",
      Descripcion:
        "Baby Back Ribs BBQ o Whiskey Glaze, Buffalo Wings, Friday’s fries, acompañadas con coleslaw y nuestros aderezos Blue Cheese y Buffalo.",
      Precio: 400.0,
      Id: "1_RIBS & WINGS BOX",
    };

    request("https://us-central1-eatfinder.cloudfunctions.net/restaurants")
      .post("/1/menu")
      .set("Content-Type", "application/json")
      .send(producto)
      .expect(200, (err, res) => {
        const { body } = res;
        expect(res.body).toMatchObject(expectedResponse);
        done();
      });
  });

  //pruebas unitarias obtener usuario
  it("Obtener bien los datos del usuario solicitado", function (done) {
    const correo = "airton123@gmail.com";
    const expectedResponse = [
      {
        id: "airton123@gmail.com",
        nombre: "Mario",
        apellido: "Obando",
        password: "12345#abc",
      },
    ];
    firestoreMock.mockGetReturn = {
      docs: [
        {
          data: () => ({
            id: "airton123@gmail.com",
            nombre: "Javier",
            apellido: "Obando",
            password: "12345#abc",
          }),
        },
      ],
    };
    request("https://us-central1-eatfinder.cloudfunctions.net/restaurants")
      .get("/User/" + correo)
      .set("Content-Type", "application/json")
      .query()
      .expect(200, (err, res) => {
        const { body } = res;
        expect(body).toMatchObject(expectedResponse);
        expect(body.id).not.toBe("");
        expect(body.nombre).not.toBe("");
        expect(body.apellido).not.toBe("");
        expect(body.password).not.toBe("");
        expect(body).toHaveProperty("nombre", body.nombre);
        expect(body).toHaveProperty("apellido", body.apellido);
        expect(body).toHaveProperty("password", body.password);
        done();
      });
  });

  it("Obtener 404 cuando se envie un usuario inexistente", function (done) {
    const correo = "test@gmail.com";
    request("https://us-central1-eatfinder.cloudfunctions.net/restaurants")
      .get("/User/" + correo)
      .set("Content-Type", "application/json")
      .query()
      .expect(404, done);
  });

  //pruebas unitarias para modificar
  it("Verificar si existe endpoint para modificar", function (done) {
    const usuario = {
      Nombre: "Mario",
      Apellido: " ",
      Password: "",
    };

    const expectedResponse = {
      id: "airton123@gmail.com",
      nombre: "Mario",
      apellido: "Obando",
      password: "12345#abc",
    };

    firestoreMock.mockAddReturn = {
      docs: [
        {
          data: () => ({
            id: "airton123@gmail.com",
            nombre: "Mario",
            apellido: "Obando",
            password: "12345#abc",
          }),
        },
      ],
    };
    request("https://us-central1-eatfinder.cloudfunctions.net/restaurants")
      .put("/User/airton123@gmail.com")
      .set("Content-Type", "application/json")
      .send(usuario)
      .expect(200, (err, res) => {
        const { body } = res;
        expect(res.body).toMatchObject(expectedResponse);
        done();
      });
  });
});
