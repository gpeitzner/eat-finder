require('firebase-functions-test')();
const { loadFeature, defineFeature } = require('jest-cucumber');
const request = require('supertest');

const feature = loadFeature('./bdd/menu.feature', { loadRelativePath: true, errors: true });

defineFeature(feature, (test) => {
    test('Obtener productos del menu del restaurante id 1', ({ given, and, when, then }) => {
        const id = '1';
        let response = [];
        given('un parametro en la solicitud con el id del restaurante', () => {
            expect(id).toBe('1');
        });

        when('enviar solicitud post al Endpoint', () => {
            const app = require('../restaurants');

            response = request(app)
                .get('/1/menu')
                .set('Content-Type', 'application/json')
                .expect(200);
        });
        then(/^El estatus de la respuesta debe ser (\d+)$/, (arg0) => {
            const app = require('../restaurants');
            response = request(app)
                .get('/1/menu')
                .set('Content-Type', 'application/json')
                .expect(arg0);
        });

        and(/^La longitud de la respuesta debe ser (\d+)$/, (arg0) => {
            const app = require('../restaurants');
            response = request(app)
                .get('/1/menu')
                .set('Content-Type', 'application/json')
                .expect(200);
        });
    });

    test('Modificar un producto del menú del restaurante 1', ({ given, when, then, and }) => {
        const id = '1';
        const response = [];
        const body = {
            'Nombre': 'MUSHROOM ONION SWISS BURGER 2',
            'Descripcion': 'La hamburguesa coronada con champiñones, cebollas salteadas, queso Suizo y mayonesa.',
            'Precio': 81.00,
            'id': '1',
        };

        given('un parametro en la solicitud con el id del restaurante', () => {
            expect(id).toBe('1');
        });

        and('el id del producto a modificar en el body', () => {
            expect(body.id).not.toBeNull();
        });
        and('Los nuevos datos a modificar en el body', () => {
            expect(body.id).not.toBe('');
            expect(body.Descripcion).not.toBe('');
            expect(body.Precio).not.toBe('');
            expect(body.Nombre).not.toBe('');
        });
        when('enviar solicitud put al Endpoint', () => {
            const app = require('../restaurants');
            request(app)
                .put('/1/menu')
                .set('Content-Type', 'application/json')
                .send(body)
                .expect(201);
        });

        then(/^El estatus de la respuesta debe ser (\d+)$/, (arg0) => {

        });

        and('La respuesta tiene que tener Nombre,Descripcion,Precio', () => {
            when('enviar solicitud put al Endpoint', () => {
                const app = require('../restaurants');
                request(app)
                    .put('/1/menu')
                    .set('Content-Type', 'application/json')
                    .send(body)
                    .expect(201, (err, res) => {
                        const { body } = res;
                        expect(body.response).toHaveProperty('Nombre', body.Nombre);
                        expect(body.response).toHaveProperty('Descripcion', body.Descripcion);
                        expect(body.response).toHaveProperty('Precio', body.Precio);
                    });
            });
        });
    });

    test('Agregar un nuevo producto', ({ given, when, then, and }) => {
        const id = '1';
        const response = [];
        const body = {
            'Nombre': 'RIBS & WINGS BOX',
            'Descripcion': 'Baby Back Ribs BBQ o Whiskey Glaze, Buffalo Wings, Friday’s fries, acompañadas con coleslaw y nuestros aderezos Blue Cheese y Buffalo.',
            'Precio': 400.00,
        };
        given('un parametro en la solicitud con el id del restaurante', () => {
            expect(id).toBe('1');
        });

        when('enviar solicitud post al Endpoint', () => {
            const app = require('../restaurants');
            request(app)
                .post('/1/menu')
                .set('Content-Type', 'application/json')
                .send(body)
                .expect(200);
        });

        then(/^El estatus de la respuesta debe ser (\d+)$/, (arg0) => {

        });

        and('La respuesta tiene que tener Nombre,Descripcion,Precio,id', () => {
            when('enviar solicitud put al Endpoint', () => {
                const app = require('../restaurants');
                request(app)
                    .post('/1/menu')
                    .set('Content-Type', 'application/json')
                    .send(body)
                    .expect(200, (err, res) => {
                        const { body } = res;
                        expect(body.response).toHaveProperty('Nombre', body.Nombre);
                        expect(body.response).toHaveProperty('Descripcion', body.Descripcion);
                        expect(body.response).toHaveProperty('Precio', body.Precio);
                        expect(body.response).toHaveProperty('Id', body.Precio);
                    });
            });
        });
    });
});

