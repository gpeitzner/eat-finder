require('firebase-functions-test')();
const { loadFeature, defineFeature } = require('jest-cucumber');
const request = require('supertest');
const feature = loadFeature('./bdd/filterName.feature', { loadRelativePath: true, errors: true });

defineFeature(feature, (test) => {
    test('Obtener restaurantes que coincidan con la busqueda "San"', ({ given, when, then, and }) => {
        let body_request;
        let response;
        const response_required = [
            {
                'descripcion': 'Dobladas',
                'direccion': 'Av. Petapa 51-57, Cdad. de Guatemala',
                'imagen': 'https://firebasestorage.googleapis.com/v0/b/eatfinder.appspot.com/o/images%20(1).jpg?alt=media&token=f4e9dd28-008f-40ee-b754-7a2cc7caab24',
                'nombre': 'Dobladas San Carlos',
                'puntuacion': 5,
                'ubicacion': {
                    'geopoint': {
                        '_latitude': 14.558808,
                        '_longitude': -90.548755,
                    },
                    'geohash': '9fxdgd27jn',
                },
                'id': '10',
            },
            {
                'descripcion': 'Restaurante',
                'direccion': 'Calz. Atanasio Tzul 50, Cdad. de Guatemala',
                'imagen': 'https://firebasestorage.googleapis.com/v0/b/eatfinder.appspot.com/o/16_543_r_0.jpg?alt=media&token=fe5e4d42-0d84-4653-8dc5-eb1c709469b5',
                'nombre': 'San Martin',
                'puntuacion': 4,
                'ubicacion': {
                    'geopoint': {
                        '_latitude': 14.56154,
                        '_longitude': -90.547378,
                    },
                    'geohash': '9fxdgdc7j5',
                },
                'id': '7',
            },
        ];
        given('una palabra como parametro de busqueda', () => {
            body_request = {
                'Nombre': 'San',
            };
            expect(body_request).toStrictEqual({
                'Nombre': 'San',
            });
        });

        when('enviar solicitud post al Endpoint', () => {
            const app = require('../restaurants');
            const body = body_request;

            request(app)
                .post('/filtroNombre')
                .set('Content-Type', 'application/json')
                .send(body)
                .expect(200, (err, res) => {
                    if (!err) {
                        expect(res.status).toEqual(200);
                        console.log(res);
                    }
                    response = res;
                    return res;
                });
        });

        then(/^El estatus de la respuesta debe ser (\d+)$/, (arg0) => {
            expect(200).toEqual(200);
        });

        and('La respuesta tiene que tener Nombre,Descripcion,Direccion,Puntuacion,Ubicacion,id,Imagen', () => {
            expect(response_required).toStrictEqual(response_required);
        });
    });
});

