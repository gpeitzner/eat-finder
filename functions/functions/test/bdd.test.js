// file 'lib/data-structures/features/steps/singly-linked-list.steps.js'
require('firebase-functions-test')();
const { loadFeature, defineFeature } = require('jest-cucumber');
const request = require('supertest');

const feature = loadFeature('./bdd/restaurants.feature', { loadRelativePath: true, errors: true });

defineFeature(feature, (test) => {
    test('Crear Promocion a Restaurante con Id 1', ({ given, and, when, then }) => {
        const id_restaurante = 1;
        const descripcion = 'Esta es una descripcion de prueba';
        let response;
        let status;
    	given('a Parameter in Request With Restaurant ID', () => {
            expect(id_restaurante).toBe(1);
    	});

    	and('Description of Promotion in the Body', () => {
            expect(descripcion).not.toBe('');
    	});

    	when('Send POST request to Endpoint', () => {
            const app = require('../restaurants');
            const body = {
                descripcion: descripcion,
            };

            response = request(app)
                .post('/createprom/1')
                .set('Content-Type', 'application/json')
                .send(body)
                .expect(200, (err, res) => {
                    if (!err) {
                        expect(res.status).toEqual(200);
                    }
                });
        });

        then(/^Response Status must be (\d+)$/, (arg0) => {

    	});
    });
});
