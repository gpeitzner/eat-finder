Feature: Filtro Nombre Restaurante

    Scenario: Obtener restaurantes que coincidan con la busqueda "San"
        Given una palabra como parametro de busqueda
        When enviar solicitud post al Endpoint
        Then El estatus de la respuesta debe ser 200
        And La respuesta tiene que tener Nombre,Descripcion,Direccion,Puntuacion,Ubicacion,id,Imagen
