Feature: Crear Promocion a Restaurante

  Scenario: Crear Promocion a Restaurante con Id 1
    Given a Parameter in Request With Restaurant ID
    And Description of Promotion in the Body 
    When Send POST request to Endpoint
    Then Response Status must be 200