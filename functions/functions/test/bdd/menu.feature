Feature:  menu restaurante 

    Scenario: Obtener productos del menu del restaurante id 1
        Given un parametro en la solicitud con el id del restaurante
        When enviar solicitud post al Endpoint
        Then El estatus de la respuesta debe ser 200
        And La longitud de la respuesta debe ser 3

    Scenario: Modificar un producto del menú del restaurante 1
        Given un parametro en la solicitud con el id del restaurante
        And   el id del producto a modificar en el body
        And  Los nuevos datos a modificar en el body
        When enviar solicitud put al Endpoint
        Then El estatus de la respuesta debe ser 201
        And La respuesta tiene que tener Nombre,Descripcion,Precio

    Scenario: Agregar un nuevo producto
        Given un parametro en la solicitud con el id del restaurante
        When enviar solicitud post al Endpoint
        Then El estatus de la respuesta debe ser 200
        And La respuesta tiene que tener Nombre,Descripcion,Precio,Id

     
