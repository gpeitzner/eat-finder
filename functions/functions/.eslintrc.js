module.exports = {
    env: {
        browser: true,
        commonjs: true,
        es2020: true,
    },
    extends: ['google'],
    parserOptions: {
        ecmaVersion: 2020,
    },
    rules: {
        'max-len': ['error', { code: 120 }],
        'indent': ['error', 4],
        'require-jsdoc': 0,
        'object-curly-spacing': ['error', 'always'],
    },
};
