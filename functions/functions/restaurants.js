const functions = require("firebase-functions");
const admin = require("firebase-admin");
const geofirestore = require("geofirestore");
const express = require("express");
const cors = require("cors");

const app = express();

// Automatically allow cross-origin requests
app.use(cors({ origin: true }));
admin.initializeApp();

app.get("/", (req, res) => {
  try {
    const { lat, lng, range = 10 } = req.query;
    const db = admin.firestore();
    // const docRef = db.collection("Restaurante");
    const resp = [];
    // const querySnapshot = await docRef.get();
    // Create a GeoFirestore reference
    const GeoFirestore = geofirestore.initializeApp(db);
    // Create a GeoCollection reference
    const geocollection = GeoFirestore.collection("Restaurante");
    // Create a GeoQuery based on a location
    const query = geocollection.near({
      center: new admin.firestore.GeoPoint(Number(lat), Number(lng)),
      radius: Number(range),
    });

    // Get query (as Promise)
    query
      .get()
      .then((value) => {
        value.docs.forEach((doc) => {
          const respObj = {};
          respObj.descripcion = doc.data()["Descripcion"];
          respObj.direccion = doc.data()["Direccion"];
          respObj.imagen = doc.data()["Imagen"];
          respObj.nombre = doc.data()["Nombre"];
          respObj.puntuacion = doc.data()["Puntuacion"];
          respObj.ubicacion = doc.data().g;
          respObj.id = doc.id;
          if (!(doc.data()["Nombre"] === undefined)) {
            resp.push(respObj);
          }
        });
        res.status(200).json(resp);
        return resp;
      })
      .catch((e) => console.log(e));
  } catch (e) {
    console.log("e", e);
    res.status(200).json([]);
  }
});

app.get("/:id", function (req, res) {
  const db = admin.firestore();
  const resp = [];

  db.collection("Restaurante")
    .doc(req.params.id)
    .get()
    .then((doc) => {
      const respObj = {};
      respObj.descripcion = doc.data()["Descripcion"];
      respObj.direccion = doc.data()["Direccion"];
      respObj.imagen = doc.data()["Imagen"];
      respObj.nombre = doc.data()["Nombre"];
      respObj.puntuacion = doc.data()["Puntuacion"];
      respObj.ubicacion = doc.data().g;
      if (!(doc.data()["Nombre"] === undefined)) {
        resp.push(respObj);
      }
      res.status(200).json(resp);
      return resp;
    })
    .catch((error) => {
      response.status(204).json({});
      return;
    });
});

app.get("/:id/rate/:restaurant", (request, response) => {
  const email = request.params.id;
  const restaurant = request.params.restaurant;
  const db = admin.firestore();
  const rateRef = db.collection("Calificacion");
  rateRef
    .where("Correo", "==", email)
    .where("Restaurante", "==", parseInt(restaurant))
    .get()
    .then((rate) => {
      if (rate.empty) {
        response.status(204).json({});
        return;
      }
      rate.forEach((rate) => {
        response.status(200).json(rate.data());
        return;
      });
    })
    .catch((err) => {
      response.status(400).send(err);
    });
});

app.post("/:id/rate/:restaurant", (request, response) => {
  const email = request.params.id;
  const restaurant = request.params.restaurant;
  const { value } = request.body;
  const db = admin.firestore();
  const rateRef = db.collection("Calificacion");
  rateRef
    .doc(email + "_" + restaurant)
    .set({
      Correo: email,
      Restaurante: parseInt(restaurant),
      Valor: parseInt(value),
    })
    .then((rate) => {
      if (rate.empty) {
        response.status(204).json({});
        return;
      }
      response.status(200).json({
        Correo: email,
        Restaurante: parseInt(restaurant),
        Valor: parseInt(value),
      });
    })
    .catch((err) => {
      response.status(400).send(err);
      return;
    });
});

app.put("/modify/:id", function (req, res) {
  const id = req.params.id;
  const descripcion = req.body.descripcion;
  const direccion = req.body.direccion;
  const imagen = req.body.imagen;
  const nombre = req.body.nombre;
  const db = admin.firestore();
  db.collection("Restaurante")
    .doc(id)
    .update({
      Descripcion: descripcion,
      Direccion: direccion,
      Imagen: imagen,
      Nombre: nombre,
    })
    .catch((error) => {});
  res.status(200).json({ status: "Ok" });
  return res;
});

app.post("/createprom/:id", function (req, res) {
  const id = req.params.id;
  const descripcion = req.body.descripcion;
  const db = admin.firestore();
  db.collection("Restaurante")
    .doc(id)
    .collection("Promocion")
    .add({
      descripcion: descripcion,
    })
    .then(function (docRef) {
      res.status(200).send({ status: "Ok", id: docRef.id });
      return res;
    })
    .catch(function (error) {
      console.error("Error adding document: ", error);
    });
});

app.put("/modifyprom/:id", function (req, res) {
  const id = req.params.id;
  const idprom = req.body.promocion;
  const descripcion = req.body.descripcion;
  const db = admin.firestore();
  db.collection("Restaurante")
    .doc(id)
    .collection("Promocion")
    .doc(idprom)
    .update({
      descripcion: descripcion,
    })
    .then(function () {
      res.status(200).send({ status: "Ok" });
      return res;
    })
    .catch(function (error) {
      console.error("Error adding document: ", error);
    });
});

app.post("/remprom/:id", function (req, res) {
  const id = req.params.id;
  const idprom = req.body.promocion;
  const db = admin.firestore();
  db.collection("Restaurante")
    .doc(id)
    .collection("Promocion")
    .doc(idprom)
    .delete()
    .then(function () {
      res.status(200).send({ status: "Ok" });
      return res;
    })
    .catch(function (error) {
      console.error("Error adding document: ", error);
    });
});

app.get("/promotions/:id", (request, response) => {
  const id = request.params.id;
  const db = admin.firestore();
  const promotionRef = db.collection("Restaurante");
  promotionRef
    .doc(id)
    .collection("Promocion")
    .get()
    .then((promotions) => {
      if (promotions.empty) {
        response.status(204).json({});
        return;
      }
      const results = [];
      promotions.forEach((promotion) => {
        results.push({
          id: promotion.id,
          description: promotion.data().descripcion,
        });
      });
      response.status(200).json(results);
      return;
    })
    .catch((err) => {
      response.status(400).send(err);
      return;
    });
});

// endpoints menus
app.get("/:id/menu", async (request, response) => {
  const db = admin.firestore();
  const restaurante = request.params.id;
  const menuref = db.collection("Restaurante");
  const resp = [];
  menuref
    .doc(restaurante)
    .collection("Menu")
    .get()
    .then((value) => {
      value.docs.forEach((doc) => {
        const respObj = {};
        respObj.Nombre = doc.data()["Nombre"];
        respObj.Descripcion = doc.data()["Descripcion"];
        respObj.Precio = doc.data()["Precio"];
        respObj.id = doc.id;
        if (!(doc.data()["Nombre"] === undefined)) {
          resp.push(respObj);
        }
      });
      response.status(200).json(resp);
      return resp;
      // response.status(200).json(doc.data());
      // return;
    })
    .catch((error) => {
      response.status(404).send(error);
      return;
    });
});

app.put("/:id/menu", async (request, response) => {
  const db = admin.firestore();
  const restaurante = request.params.id;
  const menu_R = request.body.id;
  const newNombre = request.body.Nombre;
  const newDescripcion = request.body.Descripcion;
  const newPrecio = request.body.Precio;

  const menuref = db
    .collection("Restaurante")
    .doc(restaurante)
    .collection("Menu");
  const resp = [];
  menuref
    .doc(menu_R)
    .update({
      Nombre: newNombre,
      Descripcion: newDescripcion,
      Precio: parseFloat(newPrecio),
    })
    .then((value) => {
      response.status(201).send({
        Nombre: newNombre,
        Descripcion: newDescripcion,
        Precio: parseFloat(newPrecio),
      });
      return;
    })
    .catch((err) => {
      response.status(404).send(err);
      return;
    });
});

app.post("/:id/menu", async (request, response) => {
  const { Nombre, Descripcion, Precio } = request.body;
  const db = admin.firestore();
  const restaurante = request.params.id;
  const menuRef = db
    .collection("Restaurante")
    .doc(restaurante)
    .collection("Menu");
  menuRef
    .doc(restaurante + "_" + Nombre)
    .set({
      Nombre: Nombre,
      Descripcion: Descripcion,
      Precio: parseFloat(Precio),
    })
    .then((value) => {
      if (value.empty) {
        response.status(204).json({});
        return;
      }
      response.status(200).json({
        Nombre: Nombre,
        Descripcion: Descripcion,
        Precio: parseFloat(Precio),
        Id: restaurante + "_" + Nombre,
      });
    })
    .catch((err) => {
      response.status(400).send(err);
      return;
    });
});

app.post("/filtroNombre", function (req, res) {
  const db = admin.firestore();
  const resp = [];
  const name_query = req.body.Nombre;
  console.log(name_query);
  db.collection("Restaurante")
    .get()
    .then((value) => {
      console.log(value);
      value.docs.forEach((doc) => {
        const respObj = {};
        const nombre = doc.data()["Nombre"];
        if (nombre.includes(name_query)) {
          respObj.descripcion = doc.data()["Descripcion"];
          respObj.direccion = doc.data()["Direccion"];
          respObj.imagen = doc.data()["Imagen"];
          respObj.nombre = nombre;
          respObj.puntuacion = doc.data()["Puntuacion"];
          respObj.ubicacion = doc.data().g;
          respObj.id = doc.id;
          resp.push(respObj);
        }
      });
      res.status(200).json({});
      return;
    })
    .catch((error) => {
      res.status(204).json({});
      return;
    });
});

// endpoint modify user
app.get("/User/:id", async (request, response) => {
  const db = admin.firestore();
  const usuario = request.params.id;
  const usuarioRef = db.collection("Usuario");
  const resp = [];
  usuarioRef
    .doc(usuario)
    .get()
    .then((value) => {
      console.log(value);
      const results = [];
      results.push({
        id: value.id,
        nombre: value.data().Nombre,
        apellido: value.data().Apellido,
        password: value.data().Password,
      });
      response.status(200).json(results);
      return;
    })
    .catch((error) => {
      response.status(404).send(error.message);
      return;
    });
});

app.put("/User/:id", async (request, response) => {
  const db = admin.firestore();
  const usuario = request.params.id;
  const newNombre = request.body.Nombre;
  const newApellido = request.body.Apellido;
  const newPassword = request.body.Password;

  newData = {};
  if (!(!newNombre || /^\s*$/.test(newNombre))) {
    newData.Nombre = newNombre;
  }

  if (!(!newApellido || /^\s*$/.test(newApellido))) {
    newData.Apellido = newApellido;
  }

  if (!(!newPassword || /^\s*$/.test(newPassword))) {
    newData.Password = newPassword;
  }

  const usuarioRef = db.collection("Usuario");
  const resp = [];
  usuarioRef
    .doc(usuario)
    .update(newData)
    .then((value) => {
      usuarioRef
        .doc(usuario)
        .get()
        .then((value2) => {
          response.status(200).json({
            id: value2.id,
            nombre: value2.data().Nombre,
            apellido: value2.data().Apellido,
            password: value2.data().Password,
          });
          return value2;
        }).catch((error) => {
          response.status(404).send(error);
          return;
        });
      response.status(200).json({});
      return;
    })
    .catch((error) => {
      response.status(404).send(error);
      return;
    });
});
module.exports = functions.https.onRequest(app);
